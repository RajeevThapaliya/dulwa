<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
    <meta name="author" content="">
	<title>Dulwa</title>
	
    <link href="{{ asset('/packages/admin.css') }}" rel="stylesheet" type="text/css">	
	<link href="{{ asset('/packages/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link href="{{ asset('/packages/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/packages/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/packages/plugins/iCheck/flat/blue.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/packages/plugins/morris/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('/packages/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/packages/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/packages/plugins/datepicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('/packages/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <link href="{{ asset('/packages/plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css">	
    <link href="{{ asset('/packages/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css">
	
	 <link href="{{ asset('/packages/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css">
	
	


</head>

<body class="hold-transition skin-blue sidebar-mini">

		

@if(!Request::is('auth/login'))
			@include('navigation')
			@include('main_body')
			@include('footer')
		@else
			@yield('content')
		@endif


  

  
  

	
	
<!-- jQuery 2.2.0 -->
<script src="{{ asset('/packages/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('/packages/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('/packages/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('/packages/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('/packages/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('/packages/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('/packages/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('/packages/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('/packages/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('/packages/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('/packages/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('/packages/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/packages/dist/js/app.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('/packages/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('/packages/dist/js/demo.js') }}"></script>


<script src="{{ asset('/packages/datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/packages/datatable/js/dataTables.bootstrap.min.js') }}"></script>

<script src="{{ asset('/packages/ckeditor/ckeditor.js') }}"></script>
<!--<script src="{{--{{ asset('/packages/ckfinder/ckfinder.js') }}--}}"></script>-->

<script type="text/javascript">

	$('#example1').DataTable();
	
	$('.datepicker').datepicker({
		format:"M d,yyyy"
	});
	
	
</script>
<!-- For note section text area in content create and edit view  -->
<script type="text/javascript">
	
	CKEDITOR.on("instanceReady", function(event){
		if($("#sub_type").val() =="package"){			
			$("#note_text").show();
			$("#cke_note").show();		 
		}else{
			$("#note_text").hide();
			$("#cke_note").hide();		
		}
		
		$(document).on('change','#sub_type',function(){
			if( $(this).val()=="package"){
			$("#note_text").show();
			$("#cke_note").show();
			}
			else{
			$("#note_text").hide();
			$("#cke_note").hide();
			}
		});
	});

	
	/*
	CKFinder.setupCKEditor();
     CKEDITOR.replace('textarea',
				 {
					customConfig : 'config.js',
					toolbar : 'simple',
					  
					filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
					filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
					filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
					filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
					filebrowserImageUploadUrl :'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
					filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
				 
				  
				  });
				  
	*/			  
				  
				  
				  
	
	
</script> 



   
	

</body>
</html>

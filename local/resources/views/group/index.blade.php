
@extends('app')
@section('content')

	<section class="content-header">
	  <h1>
		Groups
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="{{url('/admin/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Group</li>
	  </ol>
	</section>
		
		

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
				<div class="box-header">
					<a href="{{url('/admin/group/create')}}" class="btn btn-primary"> Add Group</a>
                </div>
                <div class="box-body">
					<div class="table-responsive">
					<table id="example1" class="table table-bordered table-hover table-striped">
					<thead>
						<th>Id</th>
						<th>Name</th>					
						<th>Created Date</th>
						<th>Action</th>
					</thead> 
					<tbody>
					<?php $i =1; ?>
						@foreach($groups as $group)
						<tr>
							<td>{{$i}}</td>
							<td>{{$group->name}}</td>
							<td>{{$group->created_at->format('M d, Y')}}</td>
							<td>
								<a href="{{ route('admin.group.edit', $group->id) }}" class="btn btn-primary pull-left">Edit</a>
								
								{!! Form::open(['method' => 'DELETE', 'route'=>['admin.group.destroy', $group->id],'onsubmit' => 'return confirm("Do you want to delete this data?")']) !!}
								{!! Form::submit('Delete', ['class' => 'btn btn-primary pull-left']) !!}
								{!! Form::close() !!}
							</td>				
						</tr>							
						<?php $i++;?>
						@endforeach
					</tbody>
					</table>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection


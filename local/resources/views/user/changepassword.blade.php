
@extends('app')
@section('content')

<section class="content-header">
          <h1>
            Change Password
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{url('/admin/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{url('/admin/changepassword')}}"> Change Password</a></li>
          </ol>
        </section>


<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
					<h3 class="box-title">Create</h3>
                </div>
                <div class="box-body">
					@if($errors->any())
						<div class="alert alert-danger">
							@foreach($errors->all() as $error)
								<p>{{ $error }}</p>
							@endforeach
						</div>
					@endif
					 
					{!! Form::open(['route' => 'admin.updatepassword']) !!}
					<div class="form-group">
						{!! Form::label('old_password', 'Old Password',['class'=>'control-label']) !!}
						{!! Form::password('old_password',array('class'=>'form-control required ','data-trigger'=>'change focusout','data-required-message'=>'Please enter old password')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('password', 'New Password',['class'=>'control-label']) !!}							
						{!! Form::password('password',array('class'=>'form-control required ','data-trigger'=>'change focusout','data-required-message'=>'Please enter password')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('confirm_password', 'Confirm Password',['class'=>'control-label']) !!}
						{!! Form::password('confirm_password',array('class'=>'form-control required','data-trigger'=>'change focusout','data-equalto'=>'#password','data-required-message'=>'Please re-enter password','data-equalto-message'=>'Password didn\'t match with confirm password')) !!}
					</div>
						{!! Form::submit('Change Password',array('class'=>'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>	
</section>

@endsection

@extends('app')
@section('content')

	<section class="content-header">
	  <h1>
		Users
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="{{url('/admin/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">User</li>
	  </ol>
	</section>
		
		

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
					<div class="box-header">
					<a href="{{url('/admin/user/create')}}" class="btn btn-primary"> Add User</a>
                </div>
                <div class="box-body">
					<div class="table-responsive">
					<table id="example1" class="table table-bordered table-hover table-striped">
					<thead>
						<th>Id</th>
						<th>Name</th>
						<th>Username</th>
						<th>Email</th>
						<th>Group</th>
						<th>Status</th>							
						<th>Created Date</th>
						<th>Action</th>
					</thead> 
					<tbody>
					<?php $i =1; ?>
						@foreach($users as $user)
						<tr>
							<td>{{$i}}</td>
							<td>{{$user->name}}</td>
							<td>{{$user->username}}</td>
							<td>{{$user->email}}</td>
							<td>
								@if(is_object($user->group))
									{{$user->group->name}}
								@endif
							</td>
							<td>{{($user->status=='1')?'Active':'Inactive'}}</td>							
							<td>{{$user->created_at->format('M d, Y') }}</td>
							<td>
								<a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-primary pull-left">Edit</a>
								{!! Form::open(['method' => 'DELETE', 'route'=>['admin.user.destroy', $user->id],'onsubmit' => 'return confirm("Do you want to delete this data?")']) !!}
								{!! Form::submit('Delete', ['class' => 'btn btn-primary pull-left']) !!}
								{!! Form::close() !!}
							</td>				
						</tr>							
						<?php $i++;?>
						@endforeach
					</tbody>
					</table>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection


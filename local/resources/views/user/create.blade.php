
@extends('app')
@section('content')

 <section class="content-header">
          <h1>
            Users
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{url('/admin/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{url('/admin/user')}}"> User</a></li>
          </ol>
        </section>
	

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
					<h3 class="box-title">Create</h3>
                </div>
                <div class="box-body">
					@if($errors->any())
						<div class="alert alert-danger">
							@foreach($errors->all() as $error)
								<p>{{ $error }}</p>
							@endforeach
						</div>
					@endif
					 
					{!! Form::open(['route' => 'admin.user.store']) !!}
						<div class="form-group">
							{!! Form::label('name', 'Name') !!}
							{!! Form::text('name',null,array('class'=>'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('username', 'Username') !!}
							{!! Form::text('username',null,array('class'=>'form-control')) !!}
							
						</div>
						<div class="form-group">
							{!! Form::label('email', 'Email') !!}
							{!! Form::text('email',null,array('class'=>'form-control')) !!}
						</div>
						
						<div class="form-group">
							{!! Form::label('phone', 'Phone') !!}
							{!! Form::text('phone',null,array('class'=>'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('gender', 'Gender',['class'=>'control-label']) !!}
							{!! Form::radio('gender', 'male',true) !!}
							{!! Form::label('gender', 'Male') !!}
							{!! Form::radio('gender', 'female') !!}
							{!! Form::label('gender', 'Female') !!}
						</div>
						<div class="form-group">
							{!! Form::label('group_id', 'Group') !!}
							{!! Form::select('group_id',array(null=>'Please Select')+$groups,null,array('class'=>'form-control')) !!}
						</div>
						<div class="form-group">
								{!! Form::label('status', 'Status',['class'=>'control-label']) !!}
								{!! Form::radio('status', '1', true) !!}
								{!! Form::label('status', 'Active') !!}
								{!! Form::radio('status', '0') !!}
								{!! Form::label('status', 'Inactive') !!}
						</div>
                        
						{!! Form::submit('Create',array('class'=>'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>	
</section>
 

@endsection
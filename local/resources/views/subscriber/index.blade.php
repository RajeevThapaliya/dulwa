
@extends('app')
@section('content')

	<section class="content-header">
	  <h1>
		Subscribers
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="{{url('/admin/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Subscriber</li>
	  </ol>
	</section>
		
		

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
						<div class="box-header">
							<!--<a href="{{--{{url('admin/subscriber/create')}}--}}" class="btn btn-primary"> Add Subscriber</a>-->
                </div>
                <div class="box-body">
					<div class="table-responsive">
					<table id="example1" class="table table-bordered table-hover table-striped">
					<thead>
						<th>Id</th>
						<th>Email</th>
						<th>Status</th>							
						<th>Created Date</th>
						<!--<th>Updated Date</th>
						<th>Action</th>-->
					</thead> 
					<tbody>
					<?php $i =1; ?>
						@foreach($subscribers as $subscriber)
						<tr>
							<td>{{$i}}</td>
							<td>{{$subscriber->email}}</td>
							<td>{{($subscriber->status==1) ? 'Active' : 'Inactive'}}</td>		
							<td>{{$subscriber->created_at->format('M d, Y')}}</td>																	
							<!-- <td>{{$subscriber->updated_at->format('M d, Y')}}</td> -->
						<!--<td>-->
							{{--@if($subscriber->status =='0')--}}
								<!--<a href="{{--{{ route('subscriber.status', array($subscriber->id,1)) }}--}}" class="btn btn-primary pull-left">Enable</a>-->
									{{--@else--}}	
								<!--<a href="{{--{{ route('subscriber.status', array($subscriber->id,0)) }}--}}" class="btn btn-primary pull-left">Disable</a>-->
								{{--@endif--}}
							{{--
								{!! Form::open(['method' => 'DELETE', 'route'=>['admin.subscriber.destroy', $subscriber->id],'onsubmit' => 'return confirm("Do you want to delete this data?")']) !!}
								{!! Form::submit('Delete', ['class' => 'btn btn-primary pull-left']) !!}
							
							{!! Form::close() !!}--}}	
								
							<!--</td>-->				
						</tr>							
						<?php $i++;?>
						@endforeach
					</tbody>
					</table>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection


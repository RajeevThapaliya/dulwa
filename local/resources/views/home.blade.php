@extends('app')

@section('content')
<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 50px;
				margin-top: 50%;
				color: #B0BEC5;
				font-weight: 100;
				font-family: 'Lato';
			}

		</style>
<div class="container">
	<div class="content">
		<div class="title">Welcome To Admin Panel</div>
	</div>
</div>
@endsection


@extends('app')
@section('content')

	<section class="content-header">
	  <h1>
		Feedbacks
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="{{url('/admin/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Feedback</li>
	  </ol>
	</section>
		
		

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
						<div class="box-header">
						<!--<a href="{{--{{url('/client_contact/create')}}--}}" class="btn btn-primary"> Add Client Contact</a>-->
                </div>
                <div class="box-body">
					<div class="table-responsive">
					<table id="example1" class="table table-bordered table-hover table-striped">
					<thead>
						<th>Id</th>
						<th>Name</th>
						<th>Email</th>
						<th>Contact</th>						
						<th>Message</th>							
						<th>Date</th>
						<th>Action</th>
					</thead> 
					<tbody>
					<?php $i =1; ?>
						@foreach($feedbacks as $feedback)
						<tr>
							<td>{{$i}}</td>
							<td>{{$feedback->name}}</td>
							<td>{{$feedback->email}}</td>
							<td>{{$feedback->contact_number}}</td>							
							<td>{{$feedback->message}}</td>		
							<td>{{$feedback->created_at->format('M d, Y')}}</td>
							<td>
								{!! Form::open(['method' => 'DELETE', 'route'=>['admin.feedback.destroy', $feedback->id],'onsubmit' => 'return confirm("Do you want to delete this data?")']) !!}
								{!! Form::submit('Delete', ['class' => 'btn btn-primary pull-left']) !!}
								{!! Form::close() !!}
							</td>				
						</tr>							
						<?php $i++;?>
						@endforeach
					</tbody>
					</table>
					
					</div>
				</div>
		</div>
		</div>
	</div>
</section>
@endsection



@extends('app')
@section('content')

 <section class="content-header">
          <h1>
            Feedbacks
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{url('/admin/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{url('/admin/feedback')}}">Feedback</a></li>
          </ol>
        </section>
	

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
					<h3 class="box-title">Create</h3>
                </div>
                <div class="box-body">
					@if($errors->any())
						<div class="alert alert-danger">
							@foreach($errors->all() as $error)
								<p>{{ $error }}</p>
							@endforeach
						</div>
					@endif
					 
					{!! Form::open(['route' => 'admin.feedback.store']) !!}
						<div class="form-group">
							{!! Form::label('name', 'Name') !!}
							{!! Form::text('name',null,array('class'=>'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('email', 'Email') !!}
							{!! Form::text('email',null,array('class'=>'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('contact_number', 'Contact') !!}
							{!! Form::text('contact_number',null,array('class'=>'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('message', 'Message') !!}
							{!! Form::textarea('message',null,array('class'=>'form-control')) !!}
						</div>
						                    
						{!! Form::submit('Create',array('class'=>'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>	
</section>
 

@endsection
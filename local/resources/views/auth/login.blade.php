@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row ">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default login" style="margin-top: 10%;">
				<div class="panel-heading">Login</div>
				<div class="panel-body">
					
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!--
						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>
						-->
						<div class="input-group">						
							<!--<label class="col-md-4 control-label sr-only">Email or Username</label>-->
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input type="text" class="form-control" name="email_or_username" value="{{ old('email_or_username') }}" placeholder="Email Address or Username">
							
						</div>
						 @if ($errors->has('email_or_username'))<p style="color:red;">{!!$errors->first('email_or_username')!!}</p>@endif
						<span class="help-block"></span>
						<div class="input-group">
						
							<!--<label class="col-md-4 control-label sr-only">Password</label>-->
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input type="password" class="form-control" name="password" placeholder="Password">
							
						</div>
						 @if ($errors->has('password'))<p style="color:red;">{!!$errors->first('password')!!}</p>@endif
						<span class="help-block"></span>
						<div class="input-group">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember"> Remember Me
								</label>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn responsive-width">Login</button>

								<!--<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>-->
							</div>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

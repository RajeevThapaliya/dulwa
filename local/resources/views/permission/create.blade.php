
@extends('app')
@section('content')

 <section class="content-header">
          <h1>
            Permissions
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{url('/admin/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{url('/admin/permission')}}"> Permissions</a></li>
          </ol>
        </section>
	

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
					<h3 class="box-title">Create</h3>
                </div>
                <div class="box-body">
					@if($errors->any())
						<div class="alert alert-danger">
							@foreach($errors->all() as $error)
								<p>{{ $error }}</p>
							@endforeach
						</div>
					@endif
					 
					{!! Form::open(['route' => 'admin.permission.store']) !!}
						<div class="form-group">
							{!! Form::label('group_id', 'Group') !!}
							{!! Form::select('group_id',array(null=>'Please Select')+$groups,null,array('class'=>'form-control')) !!}
							
						</div>
						<div class="form-group">
							{!! Form::label('module', 'Module') !!}
							{!! Form::select('module[]',array(null=>'Please Select','user'=>'User','banner'=>'Banner','content'=>'Content','package'=>'Package','packageorder'=>'Package Order','corporate'=>'Corporate','testimonial'=>'Testimonial','subscriber'=>'Subscriber','client_contact'=>'Client Contact','uploaded_cv'=>'Uploaded CV','vacancy'=>'Vacancy','department'=>'Department','download'=>'Download','category'=>'Category','news'=>'News','notice'=>'Notice','group'=>'Group','permission'=>'Permission'),null,array('class'=>'form-control','multiple' => true,'size' => '10x5')) !!}
						</div>
						<div class="form-inline">
							<div class="form-group">
								{!! Form::label('add', 'Add') !!}
								{!! Form::checkbox('add', 'yes',null,array('class'=>'checkbox')) !!}
								
							</div>
							<div class="form-group">
								{!! Form::label('edit', 'Edit') !!}
								{!! Form::checkbox('edit', 'yes',null,array('class'=>'checkbox')) !!}
								
							</div>
							<div class="form-group">
								{!! Form::label('view', 'View') !!}
								{!! Form::checkbox('view', 'yes',null,array('class'=>'checkbox')) !!}
								
							</div>
							<div class="form-group">
								{!! Form::label('delete', 'Delete') !!}
								{!! Form::checkbox('delete', 'yes',null,array('class'=>'checkbox')) !!}
								
							</div>
						</div>
						
                        
						{!! Form::submit('Create',array('class'=>'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>	
</section>
 

@endsection
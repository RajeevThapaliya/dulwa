{{--{{dd($permission)}}--}}
@extends('app')
@section('content')

	<section class="content-header">
	  <h1>
		Users
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="{{url('/admin/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Permission</li>
	  </ol>
	</section>
		
		

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
			@if(!$permission->isEmpty())
				@if($permission[0]->view=='yes')
                <div class="box-header">
				@if($permission[0]->add=='yes')
                <a href="{{url('/admin/permission/create')}}" class="btn btn-primary"> Add Permission</a>
				@endif
                </div>
                <div class="box-body">
					<div class="table-responsive">
					<table id="example1" class="table table-bordered table-hover table-striped">
					<thead>
						<th>Id</th>
						<th>Group</th>		
						<th>Created Date</th>
						<th>Updated Date</th>
						<th>Action</th>
					</thead> 
					<tbody>
					<?php $i =1; ?>
						@foreach($permissions as $permission_data)
						<tr>
							<td>{{$i}}</td>
							<td>
							@if(is_object($permission_data->group))
								{{$permission_data->group->name}}							
							@endif
							</td>					
							<td>{{$permission_data->created_at->format('M d, Y') }}</td>		
							<td>{{$permission_data->updated_at->format('M d ,Y') }}</td>
							<td>
							@if($permission[0]->edit=='yes')
								<a href="{{ route('admin.permission.edit', $permission_data->id) }}" class="btn btn-primary pull-left">Edit</a>
							@endif
							@if($permission[0]->delete=='yes')
									
								{!! Form::open(['method' => 'DELETE', 'route'=>['admin.permission.destroy', $permission_data->id],'onsubmit' => 'return confirm("Do you want to delete this data?")']) !!}
								{!! Form::submit('Delete', ['class' => 'btn btn-primary pull-left']) !!}
								{!! Form::close() !!}
							@endif	
							</td>				
						</tr>							
						<?php $i++;?>
						@endforeach
					</tbody>
					</table>
					
					</div>
				</div>
				@else
					<img src="{{ asset('access_denied.png')}}" alt="access_denied" style="width: 100%;">
				@endif
			@else
				<img src="{{ asset('access_denied.png')}}" alt="access_denied" style="width: 100%;">
			@endif
			</div>
		</div>
	</div>
</section>
@endsection


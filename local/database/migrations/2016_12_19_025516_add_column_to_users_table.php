<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('username');
			$table->string('gender');
			$table->string('phone');
			$table->integer('group_id');
			$table->string('status');
			$table->softDeletes();
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->integer('deleted_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('username');
			$table->dropColumn('gender');
			$table->dropColumn('phone');
			$table->dropColumn('status');
			$table->dropColumn('deleted_at');
			$table->dropColumn('created_by');
			$table->dropColumn('updated_by');
			$table->dropColumn('deleted_by');
		});
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrouppermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_permissions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('group_id');
			$table->integer('permission_id');			
			$table->string('add');			
			$table->string('edit');			
			$table->string('view');				
			$table->string('delete');
			$table->timestamps();
			$table->integer('created_by');
			$table->integer('updated_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_permissions');
	}

}

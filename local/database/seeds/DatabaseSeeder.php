<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('GroupTableSeeder');
		$this->call('PermissionTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('GroupPermissionTableSeeder');
		$this->call('UserGroupTableSeeder');
	}

}

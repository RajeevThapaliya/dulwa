<?php

use Illuminate\Database\Seeder;
use App\Model\GroupPermission; // to use Eloquent Model 

class GroupPermissionTableSeeder extends Seeder {

	public function run()
	{
		// clear table
        GroupPermission::truncate();
		//DB::table('permissions')->delete();

        GroupPermission::create(['group_id'=>1,
        						'permission_id'=>1,
        						'add'=>'yes',
        						'edit'=>'yes',
        						'view'=>'yes',
        						'delete'=>'yes',
			        			'created_by'=>1,
								'created_at'=>date('Y-m-d H:i:s'),
								'updated_at'=>date('Y-m-d H:i:s')]);
	}

}
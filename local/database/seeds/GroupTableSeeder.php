<?php

use Illuminate\Database\Seeder;
use App\Model\Group; // to use Eloquent Model 

class GroupTableSeeder extends Seeder {

	public function run()
	{
		// clear table
        Group::truncate(); 
		//DB::table('groups')->delete();

        Group::create(['name'=>'Admin',
        			'created_by'=>1,
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s')]);
	}

}
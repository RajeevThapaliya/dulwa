<?php

use Illuminate\Database\Seeder;
use App\Model\UserGroup; // to use Eloquent Model 

class UserGroupTableSeeder extends Seeder {

	public function run()
	{
		// clear table
        UserGroup::truncate(); 
		//DB::table('groups')->delete();

        UserGroup::create(['user_id'=>1,
			'group_id'=>1,
			'created_by'=>1,
			'created_at'=>date('Y-m-d H:i:s'),
		        'updated_at'=>date('Y-m-d H:i:s')]);
	}

}
<?php

use Illuminate\Database\Seeder;
use App\Model\User; // to use Eloquent Model 

class UserTableSeeder extends Seeder {

	public function run()
	{
		// clear table
        User::truncate();
		//DB::table('users')->delete();

        User::create(['name'=>'admin',
					'email'=>'admin@gmail.com',
					'username'=>'admin',
					'password'=>\Hash::make('admin'),
					'gender'=>'male',
					'phone'=>'',
					'group_id'=>1,
					'status'=>'1',
        			'created_by'=>1,
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s')]);
	}

}
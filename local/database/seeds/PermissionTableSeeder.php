<?php

use Illuminate\Database\Seeder;
use App\Model\Permission; // to use Eloquent Model 

class PermissionTableSeeder extends Seeder {

	public function run()
	{
		// clear table
        Permission::truncate();
		//DB::table('permissions')->delete();

        Permission::create(['module'=>'user',
        			'created_by'=>1,
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s')]);
	}

}
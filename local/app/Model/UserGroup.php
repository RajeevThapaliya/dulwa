<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model {

	protected $table = 'user_groups';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','group_id'];
	
	public function user(){
         
          return $this->belongsTo('App\User','user_id','id');
	} 

	public function group(){
         
          return $this->belongsTo('App\Group','group_id','id');
	} 

}

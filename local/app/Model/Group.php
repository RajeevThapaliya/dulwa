<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {

	protected $table = 'groups';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

}

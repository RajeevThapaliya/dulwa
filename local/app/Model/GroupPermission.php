<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupPermission extends Model {

	protected $table = 'group_permissions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['group_id', 'permission_id', 'add', 'edit','view','delete','created_by','updated_by'];

	
	public function group(){
         
          return $this->belongsTo('App\Group','group_id','id');
	} 

}

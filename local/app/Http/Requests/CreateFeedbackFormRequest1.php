<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Response;


class CreateFeedbackFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	
	public function rules()
	{
		
		
		return [
				'name' => 'required',
				'email' => 'required',
				'contact_number' => 'required|numeric'
				
		];
	}
	
	public function messages()
	{
		return [
				 'name.required' => 'Name field is required',
				 'email.required' => 'Email field is required',
				 'contact_number.required' => 'Phone field is required',
				 'contact_number.numeric' => 'Phone number must be numeric'
				
		];
	}
	
}

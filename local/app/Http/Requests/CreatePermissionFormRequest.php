<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Response;

class CreatePermissionFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	
	public function rules()
	{
		return [
				'group_id'=>'required',
				'module' => 'required',				
				 
				
		];
	}
	
	public function messages()
	{
		return [
				'group_id.required'=>'Group field is required',
				'module.required' => 'Module field is required',
				 
				
		];
	}
	
}

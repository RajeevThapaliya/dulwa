<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\Request as Req;
use Response;

class UpdateUserFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	
	public function rules(Req $request)
	{
		
		return [
				 'name' => 'required',
				 'email' => 'required|email|unique:users,email,'.$request->segment(3),
				 'username' => 'required|unique:users,username,'.$request->segment(3),
				 'group_id'=>'required',
				
		];
	}
	
	public function messages()
	{
		return [
				 'name.required' => 'Name field is required',
				 'username.required'=>'Username field is required',
				 'username.unique' => 'Username already exist',
				 'email.required' => 'Email field is required',
				 'email.unique' => 'Email already exist',
				 'email.email' => 'Enter correct email address',
				 'group_id.required'=>'Group field is required',
				
		];
	}
	
}

<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\Request as Req;
use Response;

class UpdateGroupFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	
	public function rules(Req $request)
	{
		return [
				 'name' => 'required',
				 
				
		];
	}
	
	public function messages()
	{
		return [
				 'name.required' => 'Name field is required',
				 
				
		];
	}
	
}

<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Response;


class CreateSubscriberFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	
	public function rules()
	{
		return [				 
				 'email' => 'required|email',
				
		];
	}
	
	public function messages()
	{
		return [
				 'email.required' => 'Email field is required',
				 //'email.unique' => 'Email already exist',
				 'email.email' => 'Enter correct email address',
				// 'group_id.required'=>'Group field is required',
				
		];
	}
	
}

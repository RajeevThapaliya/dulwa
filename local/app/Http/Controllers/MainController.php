<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Feedback;
use App\Model\Subscriber;
// use App\Http\Requests\CreateFeedbackFormRequest;

use Illuminate\Http\Request;

class MainController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
									
		return View('main.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function feedback(Request $request)
	{
		$client=Feedback::where('contact_number',$request->input('contact_number'))
							->get();	
		


		if($client->isEmpty()){
			$feedback = Feedback::create(['name'=>$request->input('name'),
										'email'=>$request->input('email'),
										'contact_number'=>$request->input('contact_number'),
										'message'=>$request->input('message')]);

				
			return redirect('/');
		
		}else{
	
			return redirect()->back()->withErrors(['Your contact information is already exist']);
		}
	}

	public function subscriber(Request $request)
	{	
		
		$result=Subscriber::where('email',$request->input('email'))
								->get();
								
		if($result->isEmpty()){
			$subscriber =Subscriber::create(['email'=>$request->input('email'),
										'status'=>1]);
			return redirect()->back();
		}else{
			
			return redirect()->back()->withErrors(['You are already subscribed to our newsletter.']);
		}
	
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Model\Subscriber;
use App\Model\Permission;
use App\Model\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSubscriberFormRequest;

use Illuminate\Http\Request;

class SubscriberController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$subscribers =Subscriber::orderBy('created_at', 'desc')
										->get();
										
		
		// $user=User::where('id',\Auth::user()->id)
		// 				->get();
		// if($user[0]->group_id){			
		
		//  $permission =Permission::where('group_id',$user[0]->group_id)											->where('module', 'LIKE', '%subscriber%')												->get();
			
		// }
		
		return View('subscriber.index', compact('subscribers'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		 return View('subscriber.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateSubscriberFormRequest $request)
	{
		 
		$subscriber = Subscriber::create(['email'=>$request->input('email'),
										'status'=>1]);
		 return redirect('admin/subscriber');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$result =Subscriber::destroy($id);
		return redirect('admin/subscriber');
	}
	
	public function change_status($id,$status)
	{	
		
		
		$result=Subscriber::where('id', $id)->update(['status' => $status]);
		
		
		
		$subscriber =Subscriber::where('id', $id)->get();
		/*
		if($subscriber[0]->status==1){
			//send email
		}
		*/
		return redirect('admin/subscriber');
		
	}


}

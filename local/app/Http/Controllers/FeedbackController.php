<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Model\Feedback;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateClientContactFormRequest;

use Illuminate\Http\Request;

class FeedbackController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$feedbacks = Feedback::orderBy('created_at', 'desc')
								->get();									  
			
		 return View('feedback.index', compact('feedbacks','permission'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		 return View('feedback.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateClientContactFormRequest $request)
	{
		$feedback = Feedback::create(['name'=>$request->input('name'),
										'contact_number'=>$request->input('contact_number')]);
		 return redirect('admin/feedback');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$result = Feedback::destroy($id);
		return redirect('admin/feedback');
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Model\Group;
use App\Model\User;
use App\Model\Permission;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateGroupFormRequest;
use App\Http\Requests\UpdateGroupFormRequest;

use Illuminate\Http\Request;

class GroupController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$groups = Group::all();		
		 return View('group.index', compact('groups'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{		 
		 return View('group.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateGroupFormRequest $request)
	{
		
		$group = Group::create(['name'=>$request->input('name'),							
								'created_by'=>\Auth::user()->id]);
		 return redirect('admin/group');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$group = Group::where('id',$id)
		                        ->get();
		return View('group.show', compact('group'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$group = Group::find($id);
		
		 return View('group.edit', compact('group'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,UpdateGroupFormRequest $request)
	{
		$result =Group::where('id', $id)->update(['name' => $request->input('name'),											'updated_by'=>\Auth::user()->id]);
		return redirect('admin/group');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$result =Group::destroy($id);
		return redirect('admin/group');
	}

}

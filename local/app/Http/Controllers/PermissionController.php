<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Model\Group;
use App\Model\User;
use App\Model\Permission;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePermissionFormRequest;

use Illuminate\Http\Request;

class PermissionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$permissions = Permission::with('group')
							->get();
									  
		//dd($cities);die;
		
		$user=User::where('id',\Auth::user()->id)
						->get();
		if($user[0]->group_id){			
		
			$permission =Permission::where('group_id',$user[0]->group_id)
											->where('module', 'LIKE', '%permission%')
												->get();
			
		}
			
		 return View('permission.index', compact('permissions','permission'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		 $groups=Group::get()->lists('name', 'id');		 
		 return View('permission.create', compact('groups'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreatePermissionFormRequest $request)
	{
				
		$permission_exist = Permission::where('group_id',$request->input('group_id'))
											->get();
		if($permission_exist->isEmpty()){
			
			$moduleString = implode(",", $request->get('module'));
			
			$Permission = Permission::create(['group_id'=>$request->get('group_id'),
											'module'=>$moduleString,
											'add'=>($request->get('add'))?$request->get('add'):'',
											'edit'=>$request->get('edit')?$request->get('edit'):'',
											'view'=>$request->get('view')?$request->get('view'):'',
											'delete'=>$request->get('delete')?$request->get('delete'):'']);

			return redirect('admin/permission');
		}else{
			return redirect()->back()->withErrors(['Permission for this group is already set.']);;
		}
		
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$permission = Permission::where('id',$id)
		                        ->get();
		return View('permission.show', compact('permission'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		 $permission = Permission::find($id);
		 
		$module =  explode(",", $permission->module); 
		$permission->module=$module;
			 
		 
		 $groups=Group::get()->lists('name', 'id');
		
		 return View('permission.edit', compact('permission','groups'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,CreatePermissionFormRequest $request)
	{
		$moduleString = implode(',', $request->get('module'));	

				
		
		$result = Permission::where('id', $id)->update(['group_id'=>$request->get('group_id'),
											'module'=>$moduleString,
											'add'=>$request->get('add'),
											'edit'=>$request->get('edit'),
											'view'=>$request->get('view'),
											'delete'=>$request->get('delete')]);
		return redirect('admin/permission');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$result = Permission::destroy($id);
		return redirect('admin/permission');
	}

}

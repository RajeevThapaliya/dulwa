<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Model\Group;
use App\Model\User;
use App\Model\Permission;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserFormRequest;
use App\Http\Requests\UpdateUserFormRequest;
use App\Http\Requests\ChangePasswordFormRequest;
use Illuminate\Http\Request;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::with('group')->get();
		return View('user.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		 $groups=Group::get()->lists('name', 'id');		 
		 return View('user.create', compact('groups'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateUserFormRequest $request)
	{
		$user =User::create(['name'=>$request->input('name'),
							'email'=>$request->input('email'),
							'username'=>$request->input('username'),
							'password'=>\Hash::make($request->input('username')),
							'gender'=>$request->input('gender'),
							'phone'=>$request->input('phone'),
							'status'=>$request->input('status'),
							'group_id'=>$request->input('group_id'),
							'created_by'=>\Auth::user()->id]);
		if($user){
		$request->session()->flash('success-message', 'User created Successfully.');
		 return redirect('admin/user');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::where('id',$id)
		                        ->get();
		return View('user.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		 $user = User::find($id);
		 $groups=Group::get()->lists('name', 'id');
		
		 return View('user.edit', compact('user','groups'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,UpdateUserFormRequest $request)
	{
		$result =User::where('id', $id)->update(['name' => $request->input('name'),
												'email'=>$request->input('email'),
												'username'=>$request->input('username'),
												'password'=>\Hash::make($request->input('username')),
												'gender'=>$request->input('gender'),
												'phone'=>$request->input('phone'),
												'status'=>$request->input('status'),
												'group_id'=>$request->input('group_id'),
												'updated_by'=>\Auth::user()->id]);
		if($result){
		$request->session()->flash('success-message', 'User updated Successfully.');
		 return redirect('admin/user');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$result =User::destroy($id);
		if($result){
		$request->session()->flash('success-message', 'User updated Successfully.');
		 return redirect('admin/user');
		}
	}

	public function changepassword()
	{
		
		return View('user.changepassword');
	}

	public function updatepassword(ChangePasswordFormRequest $request)
	{
		
        $user = \Auth::user();
		
		if (!\Hash::check($request->input('old_password'), $user->password)) {
			return redirect('admin/changepassword')->withErrors([
						'old_password' => 'Old password did\'nt matched with user password.'
					]);
		} else {
			
			$user->password =\Hash::make($request->input('password'));
			$user->save();

			$request->session()->flash('success-message', 'User password has been changed.');

			return redirect('/admin/user');
		}
		
	}
	
	

}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@index');
Route::post('/feedback', 'MainController@feedback');
Route::post('/subscriber', 'MainController@subscriber');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/admin', 'HomeController@index');


Route::group(['prefix' => 'admin','middleware' => ['auth']], function()
{

    Route::get('home', 'HomeController@index');
	Route::resource('group', 'GroupController');
	// Route::resource('permission', 'PermissionController');
	Route::resource('subscriber', 'SubscriberController');
	Route::resource('feedback', 'FeedbackController');
	Route::resource('user', 'UserController');
    Route::get('changepassword',['as' => 'admin.changepassword','uses' =>'UserController@changepassword']);
    Route::post('updatepassword',['as' => 'admin.updatepassword','uses' =>'UserController@updatepassword']);
	// Route::resource('news', 'NewsController');    	
	// Route::resource('subscriber', 'SubscriberController');
	// Route::resource('client_contact', 'Client_ContactController');
});
